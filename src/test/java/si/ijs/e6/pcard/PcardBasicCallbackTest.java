package si.ijs.e6.pcard;

import org.junit.Before;
import org.junit.Test;

import java.io.File;

import si.ijs.e6.S2;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


public class PcardBasicCallbackTest {
    String loadFilename = "src/test/test_measurements/esp32_generated.s2";                  //  6796L lines, end-of-file marker
    S2 s2LoadFile;
    S2.LoadStatus s2LoadStatus;

    @Before
    public void setUp() throws Exception {
        s2LoadFile = new S2();
        s2LoadStatus = s2LoadFile.load(new File(loadFilename));
    }

    @Test
    public void testLoad_BasicFileProcessing() {
        PcardBasicCallback callback = new PcardBasicCallback(s2LoadFile);
        while (s2LoadStatus.readLines(callback, false)) {
            System.out.printf("Reading... ");
        }
        boolean everythingOk = s2LoadFile.getNumErrors() == 0;
        if (s2LoadFile.getNumErrors() > 0) {
            System.out.printf(s2LoadFile.getNotes());
        }
        System.out.printf("Number of processed lines in file %s = %d\n", loadFilename, s2LoadFile.getNumProcessedLines());
        assert(everythingOk);
        assertThat(s2LoadFile.getNumProcessedLines(), is(6796L));
    }

    @Test
    public void testLoad_DataStream0Processing() {

        PcardBasicCallback callback = new PcardBasicCallback(s2LoadFile) {
            @Override
            public boolean onStreamPacket(byte handle, long timestamp, int len, byte data[]) {
                boolean superFunc = super.onStreamPacket(handle, timestamp, len, data);
                return superFunc;
            }
        };
        while (s2LoadStatus.readLines(callback, false)) {
            System.out.printf("Reading... ");
        }
        boolean everythingOk = s2LoadFile.getNumErrors() == 0;
        if (s2LoadFile.getNumErrors() > 0) {
            System.out.printf(s2LoadFile.getNotes());
        }
        assert(everythingOk);
    }
}