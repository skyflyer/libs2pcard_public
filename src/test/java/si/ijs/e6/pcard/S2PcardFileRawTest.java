package si.ijs.e6.pcard;

import org.junit.Test;

import java.io.File;
import java.text.DecimalFormat;
import java.util.HashMap;

import si.ijs.e6.S2;

import static org.junit.Assert.*;

public class S2PcardFileRawTest {
    LoadS2inRaw loader;

    @Test
    public void testRawLoadPcardFile() {
        //loader = new LoadS2inRaw("/home/matjaz/Android/projects/MobECG/last_.s2");
        loader = new LoadS2inRaw("./src/test/test_measurements/cenversionInput.s2");
    }
}

/**
 * This test was setup as a way of seeing what is written in a file, not really a unit test;
 * The test has been setup to look at a weird 4 seconds long measurement that might or might not
 * contain a missing data segment of about 1 second.
 */
class LoadS2inRaw {
    public boolean debug_everything = true;
    protected boolean parseAnnotations = false;
    /**
     * Timing support
     */
    private long
            conversionLoopStartTime,
            scanStartTime,
            conversionLoopEndTime,
            scanEndTime;

    private float adcFrequency = 0;
    /**
     * Data stream instance that will be used to unpack data packets
     */
    DataStreamProtocol.DataStream adcDataStream;
    HashMap metadata;
    S2LoadCallback s2LoadCallback;

    private String loadFilenameDataStream;
    private S2.LoadStatus s2LoadStatus;
    private S2StreamType s2StreamType = S2StreamType.Invalid;
    private S2 s2LoadFile;

;
    /**
     * Gadget revision
     */
    public FirmwareRevision gadgetFirmwareAndHardware;
    /**
     * Recording device software, e.g. "MobECG 1.8.1-69-g616eabfa (master)" in parsed form
     */
    public RecordingSoftwareDescription recordingDeviceSoftware;
    /**
     * Target start and end times that frame the conversion;
     * if targetEndTime <= targetStartTime, no upper limit is applied
     */
    private double targetStartTime = 0, targetEndTime = targetStartTime+30;

    class S2LoadCallback extends PcardBasicCallback {
        /**
         * This constructs a measurementData class with S2 file parameter which specifies data file.
         *
         * @param file Raw data input
         */
        public S2LoadCallback(S2 file) {
            super(file);
        }

        @Override
        public boolean onDefinition(byte handle, S2.SensorDefinition definition) {
            // TODO: refactor this check to libPcard
            if (handle == (byte)'e') {
                // setups sampling frequency of time alignment in [GHz] (one over nanoseconds)
                adcFrequency = (definition.samplingFrequency * 1e-9f);
                System.out.println("Frequency = "+definition.samplingFrequency+" Hz");
            }

            return super.onDefinition(handle, definition);
        }


        @Override
        public boolean onDefinition(byte handle, S2.StructDefinition definition) {
            /**
             * stream type is defined on structure definition
             */
            boolean continueReading = super.onDefinition(handle, definition);

            if (continueReading == CONTINUE_READING) {
                adcDataStream = DataStreamProtocol.createDataStream(super.getPwpStreamType());
                s2StreamType = super.getEcgStreamType();
                return CONTINUE_READING;
            }
            return INTERRUPT_READING;
        }


        long lastTime = 0;
        long lastCounter = 0;

        @Override
        public boolean onStreamPacket(byte handle, long timestamp, int len, byte[] data) {
            boolean continueReading = super.onStreamPacket(handle, timestamp, len, data);
            // process data only if within the given time limits and that parent function
            // returned an 'ok'/'continue reading' upon analyzing the data
            if ((continueReading == CONTINUE_READING) && afterRequestedTimeIntervalStart()) {
                // todo: if scanning only, packet parsing could be simplified (libPCARD does not support that yet)
                RawDataStreamPacket packet = new RawDataStreamPacket(data, timestamp);
                UnpackedEcgData unpackedEcgData = adcDataStream.unpack(packet);

                DecimalFormat form = new DecimalFormat();
                form.applyPattern("#####0.000");
                System.out.print("Packet:");
                System.out.print(" t="+ form.format(unpackedEcgData.times[0] * 1e-9));
                System.out.print(" ∆t="+ form.format((unpackedEcgData.times[0]-lastTime) * 1e-9));
                lastTime = unpackedEcgData.times[0];
                System.out.print(" c="+unpackedEcgData.rawSampleCounter);
                System.out.print(" ∆c="+(unpackedEcgData.rawSampleCounter-lastCounter));
                lastCounter = unpackedEcgData.rawSampleCounter;
                System.out.print(" data=");
                for (int i = 0; i < unpackedEcgData.values.length; ++i)
                    System.out.print((i == 0 ? " " : ", ")+unpackedEcgData.values[i]);
                System.out.println("");

                // return true (keep reading s2) if cache was not flushed yet (not full yet), false (pause reading, to process the output) otherwise
                return CONTINUE_READING;
            } else {
                return INTERRUPT_READING;
            }
        }

        @Override
        public boolean onMetadata(String key, String value) {
            boolean ret = super.onMetadata(key, value);
            if (metadata == null) {
                metadata = new HashMap<>(20);
            }
            metadata.put(key, value);
            if (key.equalsIgnoreCase(S2PcardFile.Metadata.Sensing_gadget_hardware.getString()))
                gadgetFirmwareAndHardware = super.gadgetFirmwareAndHardware;
            if (key.equalsIgnoreCase(S2PcardFile.Metadata.Recording_device_software.getString()))
                recordingDeviceSoftware = RecordingSoftwareDescription.parseString(value);
            return ret;
        }

        @Override
        public boolean onSpecialMessage(char who, char what, String message){
            boolean result = super.onSpecialMessage(who, what, message);
            if(!parseAnnotations)
                return result;

            if(what=='a')
                System.out.println("who="+who+", message="+message);
            return result && CONTINUE_READING;
        }
    }


    public LoadS2inRaw(String fname) {
        // s2; always create new instances
        s2LoadFile = new S2();
        s2LoadStatus = s2LoadFile.load(new File(fname));
        if (!s2LoadStatus.isOk()) {
            throw new RuntimeException("Failed to load S2 '"+fname+"': "+s2LoadFile.getNotes());
        }

        System.out.println("Scanning through '"+fname+"'...");
        s2LoadCallback = new S2LoadCallback(s2LoadFile);
        if (targetEndTime <= targetStartTime)
            targetEndTime = Long.MAX_VALUE;
        s2LoadCallback.setTimeIntervalInNanos((long)(targetStartTime*1e9), (long)(targetEndTime*1e9));

        while (s2LoadStatus.moreToRead()) {
            s2LoadStatus.readLines(s2LoadCallback, false);
            if (s2LoadCallback.timeIntervalDone())
                break;
        }

        System.out.println("Finished");
    }
}