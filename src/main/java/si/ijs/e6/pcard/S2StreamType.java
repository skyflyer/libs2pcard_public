package si.ijs.e6.pcard;

import java.util.HashMap;
import java.util.Map;

import si.ijs.e6.S2;

/**
 * Define stream type enum to be used in functions, but also define some properties for each type
 */
public enum S2StreamType {
    NormalStream(BleDataStreamType.NormalStream, 1, S2PcardFile.NORMAL_DATA_STREAM_HANDLE, S2PcardFile.NORMAL_DATA_STREAM_STRUCTURE),
    ExtendedStream_0(BleDataStreamType.ExtendedStream_0, 1, S2PcardFile.NORMAL_DATA_STREAM_HANDLE, S2PcardFile.EXT_DATA_STREAM_STRUCTURE),
    Invalid(BleDataStreamType.Invalid, 0, 0, "\"invalid\""),
    Undefined(BleDataStreamType.Undefined, 0, 0, "\"not read yet\"");

    /**
     * Streams in s2 actually just mimic the way data is received in packets via BLE. Therefore
     * use the basic data stream definition here.
     */
    BleDataStreamType mimicPcardBleDataStreamType;
    /**
     * The identifier (handle) of ECG stream (comprising samples and helpers) in the s2 file.
     * Helpers include counter(s), ... (possibly others in future)
     */
    public byte ecgStreamHandle;
    /**
     * The identifier (handle) of ECG sample (or packet) counter in the s2 file.
     * Currently this is a constant.
     */
    public final byte ecgCounterHandle = S2PcardFile.ECG_SAMPLE_COUNTER_HANDLE;
    /**
     * Minimum version (int) of s2 file that can contain this type of stream.
     */
    public int minVersion;
    /**
     * Structure of the stream data for s2 files.
     */
    public String streamStructure;
    /**
     * Multiplier for timestamps; defines the time precision; currently the same for all streams.
     */
    final double timestampMultiplier = 1e-6; // us
    /**
     * The identifier (handle) of ECG samples in the file
     */
    public final byte ecgSampleHandle = (byte)'e';

    /**
     * Basic constructor
     *
     * @param mimicPcardBleDataStreamType  The pcard data stream type to mimic with s2 stream
     * @param fileVersion               Stream is only available on S2/PCARD files of this version or higher
     * @param ecgStreamHandle              Handle (index) of the stream
     * @param streamStructure           Structure of the stream (sequence of 'sensor' handles)
     */
    S2StreamType(BleDataStreamType mimicPcardBleDataStreamType, int fileVersion, int ecgStreamHandle, String streamStructure) {
        this.mimicPcardBleDataStreamType = mimicPcardBleDataStreamType;
        this.minVersion = fileVersion;
        this.ecgStreamHandle = (byte) ecgStreamHandle;
        this.streamStructure = streamStructure;
    }

    /**
     * Get the value of the stream handle for the struct and timestamp definitions.
     *
     * Example:
     *  S2.S2StoreStatus ...
     *  S2StreamType streamtype ...
     *  ...
     *  byte h = streamType.getStructDefinitionHandle();
     *  s2storeStatus.addDefinition(handle, streamType.generateStructDefinition());
     *  s2storeStatus.addDefinition(handle, streamType.generateTimestampDefinition());
     *
     * @return a valid handle for the data stream structure
     */
    public byte getStructDefinitionHandle() {
        return ecgStreamHandle;
    }

    /**
     * Generate a struct definition to store in s2
     * @return new struct definition instance for this stream
     */
    public S2.StructDefinition generateStructDefinition() {
        return new S2.StructDefinition(S2PcardFile.ECG_STREAM_NAME, streamStructure);
    }

    /**
     * Generate a timestamp definition to store in s2
     * @return new timestamp definition instance for this stream
     */
    public S2.TimestampDefinition generateTimestampDefinition() {
        // make timestamp relative, 3 bytes long, and in microseconds (can store 0 - 16.77 seconds)
        return new S2.TimestampDefinition(S2.AbsoluteId.abs_relative, (byte)3, timestampMultiplier);
    }

    /**
     * Generate an array of sensor definitions that are used by this stream.
     *
     * Example of use:
     *  S2.S2StoreStatus ...
     *  S2StreamType streamType ...
     *  GadgetMeta gadgetMeta ...
     *  ...
     *  Map&lt;Byte, S2.SensorDefinition&gt; sds = streamType.generateSensorDefinitions(
     *          gadgetMeta.getAdcTransformation(), gadgetMeta.getSamplingFrequency());
     *  for (Map.Entry&lt;Byte, S2.SensorDefinition&gt; sd: sds.entrySet())
     *      s2storeStatus.addDefinition(sd.getKey(), sd.getValue());
     *  ...
     *
     * @param ecgFunc           the linear transformation function as input
     * @param samplingFrequency the sampling frequency as input
     * @return new array of sensor definition instances
     */
    public Map<Byte, S2.SensorDefinition> generateSensorDefinitions(LinearTransformationFunction ecgFunc, float samplingFrequency) {
        S2.SensorDefinition sd_e = new S2.SensorDefinition(S2PcardFile.ECG_SAMPLE_HANDLE_NAME);
        sd_e.setUnit(ecgFunc.getUnit(), ecgFunc.getK(), ecgFunc.getN());
        sd_e.setScalar(10, S2.ValueType.vt_integer, S2.AbsoluteId.abs_absolute, 0);
        sd_e.setSamplingFrequency(samplingFrequency);

        S2.SensorDefinition sd_c = new S2.SensorDefinition(S2PcardFile.ECG_SAMPLE_COUNTER_HANDLE_NAME);
        sd_c.setUnit(S2PcardFile.ECG_SAMPLE_COUNTER_HANDLE_UNIT, 1, 0);
        sd_c.setScalar(10, S2.ValueType.vt_integer, S2.AbsoluteId.abs_absolute, 0);
        sd_c.setSamplingFrequency(0);

        return new HashMap<Byte, S2.SensorDefinition>() {
            {
                put((byte) S2PcardFile.ECG_SAMPLE_HANDLE, sd_e);
                put((byte) S2PcardFile.ECG_SAMPLE_COUNTER_HANDLE, sd_c);
            }
        };
    }

    /**
     * Get the number of bits used to store a single ECG sample
     * @return bit width
     */
    public int getSampleBitWidth() {
        return mimicPcardBleDataStreamType.getSampleBitWidth();
    }

    /**
     * Get the bit width of counters
     * @return bit width
     */
    public int getCounterBitWidth() {
        return mimicPcardBleDataStreamType.getPerPacketCounterBitWidth();
    }

    /**
     * Get the increment value for counter between succeeding packets.
     * @return the increment value
     */
    public int getCounterIncrement() {
        return mimicPcardBleDataStreamType.getPerPacketCounterIncrement();
    }

    public int getNumSamplesPerPacket() {
        return mimicPcardBleDataStreamType.numSamplesPerPacket;
    }

    /**
     * Get the pcard BLE data packet stream type
     * @return the pcard BLE data stream type
     */
    public BleDataStreamType getPcardDataStreamType() {
        return mimicPcardBleDataStreamType;
    }
}
