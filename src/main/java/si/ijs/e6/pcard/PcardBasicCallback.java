package si.ijs.e6.pcard;

import si.ijs.e6.*;
import si.ijs.e6.S2.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static si.ijs.e6.pcard.S2PcardFile.*;


/*
    TODO: comment
 */

/**
 * This class can be passed to LoadStatus as set of callbacks and will perform PCARD specific parsing of s2 file.
 *
 * Only the following parsing will be performed:
 * <ul>
 *     <li>whether the file s2 version is correct (reading will stop if not)</li>
 *     <li>reconstruction of data packet timestamps (actually done higher in the hierarchy)</li>
 *     <li>filtering by timestamp</li>
 *     <li>Linear transformation functions for data streams parsed from definitions and kept</li>
 *     <li>parsing of definitions to determine the type of packets stored (normal/extended)</li>
 *     <li>detection and flagging of disconnects</li>
 * </ul>
 *
 * @author Matjaž
 */
public class PcardBasicCallback implements ReadLineCallbackInterface {
    /**
     * This class implements a simple S2 Version.
     * For now version info is just stored but not directly used anywhere.
     */
    public static class S2FileVersion {
        private ThreePartVersion baseVersion;
        // incrementalVersionNumber is the same as baseVersionString but represented as integer (s2 translates between the two representations)
        private int incrementalVersionNumber;
        private String extendedVersion;

        /**
         * This constructs a extendedVersion with int and string type of extendedVersion
         * @param incrementalVersionNumber  the int representing base version
         * @param baseVersionString 		the string representing base version
         * @param extendedVersion 	        the string representing the extended version
         */
        public S2FileVersion(int incrementalVersionNumber, String baseVersionString, String extendedVersion) {
            this.incrementalVersionNumber = incrementalVersionNumber;
            baseVersion = new ThreePartVersion(baseVersionString);
            this.extendedVersion = extendedVersion;
        }

        /**
         * This returns int type of base version
         * @return this int base version
         */
        public String getBaseVersionString() {
            return baseVersion.getRawString();
        }

        public ThreePartVersion getBaseVersion() {
            return baseVersion;
        }

        /**
         * This returns string type of extended version
         * @return this string type of extended version
         */
        public String getExtendedVersion() {
            return extendedVersion;
        }

        /**
         * Return the extendedVersion as integer number (this is what is actually written in the file)
         * @return an integer representation of base version
         */
        public int getVersionIndex() {
            return incrementalVersionNumber;
        }

        /**
         * Check whether the version is a valid Pcard S2 version
         * @return true if the file is valid PCARD file
         */
        public boolean isValidPcard() {
            return S2PcardFile.Version.isValidPcard(incrementalVersionNumber, extendedVersion);
        }

        /**
         * Get the internal incremental (integer) version number.
         * @return the version as integer
         */
        public int getIncrementalVersionNumber() {
            return incrementalVersionNumber;
        }
    }

    /**
     * Variables used in the callbacks for filtering by time.
     * Start and End variables define the interval for which the loading will be processing data
     * (endpoints included).
     * Variables are protected to allow classes down the hierarchy to use them directly.
     */
    protected long  loadStartNanos = 0,
                    loadEndNanos = Long.MAX_VALUE;

    protected S2 s2;
    // linear transformation functions for the data streams (mapped by ecgStreamHandle)
    private Map<Integer, LinearTransformationFunction> linearTransformationFunctions = new HashMap<>();

    /**
     * When s2 version is read it is stored here and extended classes can check it from here.
     */
    public S2FileVersion s2FileVersion;
    /**
     * When hardware is determined (appropriate metadata entry is read), this variable is initialized
     */
    public FirmwareRevision gadgetFirmwareAndHardware = new FirmwareRevision("");
    /**
     * When software is determined (appropriate metadata entry is read), this variable is initialized
     */
    public RecordingSoftwareDescription recordingDeviceSoftware = new RecordingSoftwareDescription();

    /**
     * global reconnect will be flagged if a corresponding message is read from S2 file;
     * This coounter will count number of ECG packets since the disconnect. Therefore a disconnect
     * can be tested against in data stream callback by checking whether this variable equals 0.
     */
    public int numEcgPacketsAfterGlobalReconnectFlagged = 0;

    /**
     * Flag that indicates that the loaded file was created with old MobECG, which did not store
     * connect and disconnect events. This makes the file a bit harder to decode (long pauses
     * can be misidentified as disconnects)
     */
    private boolean doesNotSupportDisconnectNotifications = false;
    /**
     * The type of packets stored in file is determined when definitions are read and is used when
     * data streams are parsed
     */
    private S2StreamType filePacketType = S2StreamType.Undefined;
    /**
     * Start time of the measurements, as read from metadata, with possible pause at the beginning
     */
    private Calendar startTime;
    private String dateString, timeString, timezoneString;


    /**
     * This constructs a measurementData class with S2 file parameter which specifies data file.
     * @param file Raw data input
     */
    public PcardBasicCallback(S2 file) {
        s2 = file;
    }

    /**
     * Returns the version entry of the s2 file.
     * @return the s2FileVersion entry (initialized as soon as the file's first line is read)
     */
    public S2FileVersion getVersion() {
        return s2FileVersion;
    }

    /**
     * Get the type of ECG stream that was detected in the file
     * @return NotReadYet if stream was not read yet or a valid stream tye or Invalid
     */
    public S2StreamType getEcgStreamType() {
        return filePacketType;
    }

    /**
     * Get the ble (PWP) data stream type that is packed into s2 stream type
     * @return the enum describing this stream type
     */
    public BleDataStreamType getPwpStreamType() {
        return filePacketType.mimicPcardBleDataStreamType;
    }

    /**
     * Get the built-in linear transformation function (it will be initialized when the proper definition in s2 is read)
     *
     * @param streamHandle target stream handle to get linear transform for
     * @return the Linear transformation function for the selected data stream
     */
    public LinearTransformationFunction getLinearTransformation(int streamHandle) {
        return linearTransformationFunctions.get(streamHandle);
    }

    /**
     * Get the built-in linear transformation function (it will be initialized when the proper definition in s2 is read)
     *
     *
     * @return the Linear transformation function for the selected data stream
     */
    public LinearTransformationFunction getLinearTransformationForEcgSamples() {
        return linearTransformationFunctions.get((int)filePacketType.ecgSampleHandle);
    }

    /**
     * Set the time interval from which the load will take place; all data and other time-based lines will be loaded only if it appears within this interval
     * @param start start of the time interval (included)
     * @param end   end of the interval (included)
     */
    public void setTimeIntervalInNanos(long start, long end) {
        loadStartNanos = Math.max(0, Math.min(Long.MAX_VALUE, start));
        loadEndNanos = Math.max(0, Math.min(Long.MAX_VALUE, end));
    }

    /**
     * Set the time interval from which the load will take place; all data and other time-based lines will be loaded only if it appears within this interval
     * @param start start of the time interval (included)
     * @param end   end of the interval (included)
     */
    public void setTimeInterval(Nanoseconds start, Nanoseconds end) {
        loadStartNanos = start.getValue();
        loadEndNanos = end.getValue();
    }

    /**
     * Check if currently read line is within the interval that was requested for processing.
     * @return true if the file was read past the start timestamp and data should be processed
     */
    public boolean afterRequestedTimeIntervalStart() {
        return (s2.getTimeState() >= loadStartNanos);
    }

    /**
     * Check if currently read line is within the interval that was requested for processing.
     * @return true if the file is to be read more (not past last requested timestamp yet)
     */
    public boolean beforeRequestedTimeIntervalEnd() {
        return (s2.getTimeState() < loadEndNanos);
    }

    /**
     * Check whether the set time interval has been fully read (current timestamp is already past the interval end)
     * @return true if no more loading can be done
     */
    public boolean timeIntervalDone() {
        return s2.getTimeState() > loadEndNanos;
    }

    /**
     * Forward the query to s2: return the time state
     * @return  the value of time state (last timestamp read) in nanoseconds
     */
    public long getTimeState() {
        return s2.getTimeState();
    }

    /**
     * Checks whether datetime fields have been read and when they are, constructs a calendar object
     */
    private void checkDateFull() {
        if ((dateString != null) && (timeString != null)) {
            try {
                SimpleDateFormat sdf;
                startTime = new GregorianCalendar();
                if (timezoneString != null) {
                    sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);
                    startTime.setTime(sdf.parse(dateString + 'T' + timeString+timezoneString));
                } else {
                    sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                    startTime.setTime(sdf.parse(dateString + 'T' + timeString));
                }
            } catch (ParseException e) {
                // TODO: do something meaningful with this exception
                // a silent exception handler, oops
            }
        }
    }

    /**
     * Query for the measurement start time
     * @return a calendar instance representing the measurement start time
     */
    public Calendar getStartDateTime() {
        return startTime;
    }


    //region ReadLineCallbackInterface implementation
    @Override
    public boolean onComment(String comment) {
        return CONTINUE_READING;
    }

    @Override
    public boolean onVersion(int versionInt, String extendedVersion) {
        s2FileVersion = new S2FileVersion(versionInt, S2.getStringVersion(versionInt), extendedVersion);
        return (s2FileVersion.isValidPcard() ? CONTINUE_READING : INTERRUPT_READING);
    }

    @Override
    public boolean onSpecialMessage(char who, char what, String message) {
        // disconnects are recorded using special message
        if (S2PcardFile.DisConnectEvents.isMessageDisOrConnect(who, what, message) !=
                S2PcardFile.DisConnectEvents.MessageIs.Other)
        {
            numEcgPacketsAfterGlobalReconnectFlagged =0;
        }
        // notes from recording device sometimes contain data of interest
        if ((DeviceType.dt_recordingDevice.byteId == who) && (S2.MessageType.mt_note.byteId == what)) {
            String[] keyValueSplit = message.split("=", 2);
            if (keyValueSplit.length == 2) {
                keyValueSplit[0]=keyValueSplit[0].trim();
                keyValueSplit[1]=keyValueSplit[1].trim();

                if (keyValueSplit[0].equals(S2PcardFile.Metadata.Recording_device_software.getString())) {
                    parseRecordingDeviceSoftwareVersion(keyValueSplit[1]);
                }
            }
        }
        return CONTINUE_READING;
    }

    @Override
    public boolean onMetadata(String key, String value) {
        switch(Metadata.fromString(key)) {
            case Gadget_firmware_revision:
                gadgetFirmwareAndHardware = S2PcardFile.parseMetadataString(value);
                updateLinearTransformationFunction();
                break;
            case Recording_device_software:
                parseRecordingDeviceSoftwareVersion(value);
                break;
            case Sensing_gadget_hardware:
                gadgetFirmwareAndHardware = S2PcardFile.parseMetadataString(value);
                updateLinearTransformationFunction();
                break;
            case Mac_address:
                break;
            case Recording_date:
                dateString = value;
                checkDateFull();
                break;
            case Recording_time:
                timeString = value;
                checkDateFull();
                break;
            case Recording_timezone:
                timezoneString = value;
                checkDateFull();
                break;
            case Selected_adc_channel:
                break;
            case Debug_selected_analog_ampl:
                break;
            case Debug_selected_ecg_range:
                break;
            case Debug_calibration_coeffs:
                break;
            case Selected_transfer_protocol:
                break;
            case Recording_device_model:
                break;
            case Recording_device_os:
                break;
            case Recording_device_serial_number:
                break;
            case Think_ehr_id:
                break;
            case Sensing_gadget_location:
                break;
            case Invalid:
                break;
        }

        return CONTINUE_READING;
    }

    @Override
    public boolean onEndOfFile() {
        return INTERRUPT_READING;
    }

    @Override
    public boolean onUnmarkedEndOfFile() {
        return INTERRUPT_READING;
    }

    @Override
    public boolean onDefinition(byte handle, SensorDefinition definition) {
        return CONTINUE_READING;
    }

    @Override
    public boolean onDefinition(byte handle, StructDefinition definition) {
        // when structure ecgStreamHandle is received it is checked whether it stands for the PCARD normal packet type
        // under PWP definition;
        if (handle == 0) {
            if (checkStructureDefinitionAgainstS2StreamType(definition, S2StreamType.NormalStream))
                filePacketType = S2StreamType.NormalStream;
            else if (checkStructureDefinitionAgainstS2StreamType(definition, S2StreamType.ExtendedStream_0))
                filePacketType = S2StreamType.ExtendedStream_0;
            else {
                // unknown structure, continue reading assuming this was not an ecg-related definition
                return CONTINUE_READING;
            }
            // sensor definition must already be defined
            SensorDefinition e = s2.getEntityHandles(filePacketType.ecgSampleHandle).sensorDefinition;
            linearTransformationFunctions.put((int)filePacketType.ecgSampleHandle, new LinearTransformationFunction(e.k, e.n, e.unit));
            updateLinearTransformationFunction();
        }
        return CONTINUE_READING;
    }

    @Override
    public boolean onDefinition(byte handle, TimestampDefinition definition) {
        return CONTINUE_READING;
    }

    @Override
    public boolean onTimestamp(long nanoSecondTimestamp) {
        return (s2.getTimeState() < loadEndNanos) ? CONTINUE_READING : INTERRUPT_READING;
    }

    @Override
    public boolean onStreamPacket(byte handle, long timestamp, int len, byte data[]) {
        // if this packet is within the selected range of measurement to load, process it
        if (afterRequestedTimeIntervalStart()) {
            if ((S2StreamType.NormalStream == filePacketType) && (S2StreamType.NormalStream.ecgStreamHandle == handle)) {
                ++numEcgPacketsAfterGlobalReconnectFlagged;
                // a known packet type
            } else {
                // error: unknown packet type was defined, cannot parse it
                return INTERRUPT_READING;
            }
        }
        return beforeRequestedTimeIntervalEnd() ? CONTINUE_READING : INTERRUPT_READING;
    }

    @Override
    public boolean onUnknownLineType(byte type, int len, byte data[]) {
        return CONTINUE_READING;
    }

    @Override
    public boolean onError(int lineNum,  String error) {
        return CONTINUE_READING;
    }
    //endregion

    //region helper functions
    /**
     * Update the linear transformation function for handle 0; using existing function and firmware info
     * This function is used after reading definitions and after reading gadget version
     */
    private void updateLinearTransformationFunction() {
        LinearTransformationFunction ltf =  linearTransformationFunctions.get((int)filePacketType.ecgSampleHandle);
        if (ltf != null)
            ltf.copyFrom(S2PcardFile.getBetterLinearTransformationFunction(ltf, gadgetFirmwareAndHardware));

    }

    /**
     * Compare the provided structure definition with the provided S2StreamType if it matches.
     * @param definition the input definition
     * @param type       the input S2 stream type instance
     * @return true if they match
     */
    private boolean checkStructureDefinitionAgainstS2StreamType(StructDefinition definition, S2StreamType type) {
         return (definition.elementsInOrder.equals(type.streamStructure) &&
                    (s2.getEntityHandles(type.ecgSampleHandle) != null) &&
                    (s2.getEntityHandles(type.ecgCounterHandle) != null) &&
                    (s2.getEntityHandles(type.ecgSampleHandle).sensorDefinition != null) &&
                    (s2.getEntityHandles(type.ecgCounterHandle).sensorDefinition != null) &&
                    (s2.getEntityHandles(type.ecgSampleHandle).sensorDefinition.resolution == type.getSampleBitWidth()) &&
                    (s2.getEntityHandles(type.ecgCounterHandle).sensorDefinition.resolution == type.getCounterBitWidth()));
    }

    /**
     * Parse the recording device (e.g. the smartphone) software version (e.g. MobECG 1.2.3 ...).
     * This is used when parsing metadata.
     *
     * @param metadataValue the value to parse (usually coming from read metadata)
     */
    public void parseRecordingDeviceSoftwareVersion(String metadataValue) {
        recordingDeviceSoftware = RecordingSoftwareDescription.parseString(metadataValue);
        // immediately setup the important flags
        doesNotSupportDisconnectNotifications = !(recordingDeviceSoftware.softwareVersion.minVersion(1, 7, 9));
    }
    //endregion
}
