package si.ijs.e6.pcard;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Logger;

import si.ijs.e6.S2;


/**
 * A class that merges functionalities from libraries libS2Pcard (libS2, libPcard), libTimeAlign,
 * libFileExport (Ishne) to provide for simple file conversion from .s2 to .ishne.
 *
 * Instances of this class are able to iteratively convert s2 to ishne, that is by converting small
 * chunks and thus have low memory requirements.
 *
 * A 'chunk' is defined as a set of samples (can be in form of packets or as a timestream), that
 * are sorted in time and are processes in single iteration. A chunk is passed from s2 reader to
 * time alignment (where usually 2 chunks will reside in memory at once), to flattener (where
 * packets are converted to flat time series (temporarily holds a chunk), to resampler
 * (temporarily holds a resampled chunk) to file export (holds a copy of chunk). At once it should be assumed up to
 *
 * xample of procedure:
 *  - create and setup S2 instance for loading
 *  - create TimeAlignment instance to align packets as hey are read from s2
 *  - create Resampler to resample packets after they are read
 *  - create IshneFile instance for storing
 *  - setup loop:
 *      - read lines until first data stream line is encountered.
 *  - setup TimeAlignment instance, using sampling frequency
 *  - scanning loop:
 *      - read lines until end of file
 *      - pass timestamps and counters from data stream 0 to TimeAlignment instance
 *  - use TimeAlignment instance to calculate total data length
 *  - setup Resampler using sampling frequency
 *  - setup IshneFile instance using sampling frequency and total data length
 *  - conversion loop:
 *      - rewind file and read until end of file
 *      - pass data to TimeAlign instance,
 *          then its data to Resampler instance
 *          then its data to IshneFile instance
 **
 * TODO:
 * resampling will not start at the first sample (unless first sample is at time 0), but rather
 * somewhere between first and second sample; it is synchronized to overlap at time 0; change to be
 * synchronized to the first existing sample; targetNumberOfSamples does not factor in this offset;
 * writing to ishne does not factor in this offset when writing "record time"
 * TODO: separate scanning and conversion loops; easier to use scan results by the caller
 */
public class S2Load {
    private static final Logger logger = Logger.getLogger(S2Load.class.getName());

    public boolean debug_everything = true;
    /**
     * Timing support
     */
    private long
            conversionLoopStartTime,
            scanStartTime,
            conversionLoopEndTime,
            scanEndTime;

    private String loadFilenameDataStream;
    private S2.LoadStatus s2LoadStatus;
    private S2StreamType s2StreamType = S2StreamType.Invalid;
    private S2 s2LoadFile;
    private S2LoadCallback s2LoadCallback;
    /**
     * ScanResults hold the results of the scan (instantiated when #scan is called)
     */
    private FileScanResults scanResults;
    /**
     * Resampler will take input samples of given input frequency (as loaded from s2) and resample
     * them with target frequency (as determined by the user provided callback)
     */
    private ResampleInterface resampler;
    /**
     * The resample factor that wil lbe passed to the resampler; it is cached here mostly just to
     * be compared to 1, in which case no resampling is performed but samples are rather passed
     * from input to output unmodified.
     *
     * Time increment of resampled data equals resamplerFactor \times original_time_increment
     */
    private double resamplerFactor = 1;
    /**
     * An array to hold time aligned samples.
     */
    private float alignedSamples[];
    /**
     * An array to hold resampled time aligned samples.
     */
    private float resampledSamples[];
    /**
     * The targeted number of samples a buffer should hold
     * The default value is set here - it will result in memory usage of several MB; this value
     * can be overridden by the user.
     */
    private int targetNumSamplesInChunk = 100000;
    /**
     * Target start and end times that frame the conversion;
     * if targetEndTime <= targetStartTime, no upper limit is applied
     */
    private double targetStartTime = 0, targetEndTime = targetStartTime;
    /**
     * Time align algorithm for PCARD.
     */
    private PcardTimeAlignment<S2PacketForTimeAlign> timeAlign = new PcardTimeAlignment<>();

    public class AnnotationData{
        public long timestamp;
        public char who;
        public String message;

        AnnotationData(long time, char who, String message){
            this.timestamp = time;
            this.who = who;
            this.message = message;
        }
    }

    /**
     * Helper class for sharing information from s2 callbacks to S2 load callbacks.
     *
     * One instance should be created, filled in {@link S2LoadCallback} and further modified in
     * {@link #convertLoop}.
     */
    public class FileScanResults {
        /**
         * All read metadata is stored here
         */
        public Map<String, String> metadata;
        /**
         * Data stream instance that will be used to unpack data packets
         */
        public DataStreamProtocol.DataStream dataStream;
        /**
         * Gadget revision
         */
        public FirmwareRevision gadgetFirmwareAndHardware;
        /**
         * Recording device software, e.g. "MobECG 1.8.1-69-g616eabfa (master)" in parsed form
         */
        public RecordingSoftwareDescription recordingDeviceSoftware;
        /**
         * input sampling frequency for ECG
         */
        public float inputFrequency;
        /**
         * Linear transformation function for transforming ECG raw vales to mV
         */
        public LinearTransformationFunction linearTransformationFunction;
        /**
         * Number of input samples that will be read, transformed, and resampled
         */
        public long numInputSamples;
        /**
         * Input sample counters (first and last)
         */
        public long inputSampleCounterStart, inputSampleCounterEnd;
        /**
         * First sample time in nanoseconds (offset from measurement start)
         */
        public long startTimeNanos;
        /**
         * Last sample time in nanoseconds (offset from measurement start)
         */
        public long endInputNanos;
        /**
         * Pre-calculated number of samples (using input and output frequencies and number of input samples)
         */
        public long targetNumSamples;
        /**
         * Date and time of recording start
         */
        public Calendar recordDateTime;
        /**
         * The list of read annotations (populated during scan)
         */
        public LinkedList<AnnotationData> annotations = new LinkedList<>();
        /**
         * Get s ahort descriptive string about the hardware (gadget)
         * @return a short string describing the hardware
         */
        public String getShortHardwareDescriptionString() {
            return gadgetFirmwareAndHardware.getHardware().toString()+"\nFW "+
                    gadgetFirmwareAndHardware.getRevisionString()+"\n"+
                    recordingDeviceSoftware.getName()+" "+recordingDeviceSoftware.getVersion().toString();
        }
    }
    /**
     * Basic container for packet loaded from s2 - only holds timestamp and counter, but not data
     */
    class S2PacketForTimeAlign implements TimestampPair {
        long timestamp, counter;

        public S2PacketForTimeAlign(S2PacketForTimeAlign other) {
            timestamp = other.getTimestamp();
            counter = other.getCounter();
        }

        public S2PacketForTimeAlign(long t, long c) {
            timestamp = t;
            counter = c;
        }

        @Override public long getTimestamp()            { return timestamp;     }
        @Override public long getCounter()              { return counter;       }
        @Override public void setTimestamp(long value)  { timestamp = value;    }
        @Override public void setCounter(long value)    { counter = value;      }
    }

    /**
     * Container for a single packet read from S2 file; compatible with TimeAlign
     */
    class FullS2Packet extends S2PacketForTimeAlign {
        int data[];

        public FullS2Packet(FullS2Packet other) {
            super(other);
            this.data = other.data;
        }

        public FullS2Packet(long t, long c, int data[]) {
            super(t, c);
            this.data = data;
        }
    }

    /**
     * Callback that is used for scanning s2 file or loading it in multiple chunks
     *
     * Mode 1 - scan: used to perform a scan that calculates measurement length
     * Mode 2 - load: used to load the file a chunk of adjustable size at a time
     */
    class S2LoadCallback extends PcardBasicCallback {
        public FileScanResults fileScanResults = new FileScanResults();

            /**
         * flag that makes callback scan for length only
         */
        boolean performScanOnly;

        /**
         * This constructs a measurementData class with S2 file parameter which specifies data file.
         *
         * @param file Raw data input
         */
        public S2LoadCallback(S2 file) {
            super(file);
        }

        public void performScanOnly(boolean scanOnly) {
            this.performScanOnly = scanOnly;
        }

        /**
         * Reset internal variables and make instance available for reuse.
         */
        public void reset() {
            fileScanResults.dataStream = null;
        }

        @Override
        public boolean onDefinition(byte handle, S2.SensorDefinition definition) {
            // TODO: refactor this check to libPcard
            if (handle == (byte)'e') {
                // setups sampling frequency of time alignment in [GHz] (one over nanoseconds)
                timeAlign.setupConstantFrequency(definition.samplingFrequency * 1e-9);
            }

            return super.onDefinition(handle, definition);
        }


        @Override
        public boolean onDefinition(byte handle, S2.StructDefinition definition) {
            /**
             * stream type is defined on structure definition
             */
            boolean continueReading = super.onDefinition(handle, definition);

            if (continueReading == CONTINUE_READING) {
                fileScanResults.dataStream = DataStreamProtocol.createDataStream(super.getPwpStreamType());
                s2StreamType = super.getEcgStreamType();
                return CONTINUE_READING;
            }
            return INTERRUPT_READING;
        }

        @Override
        public boolean onStreamPacket(byte handle, long timestamp, int len, byte[] data) {
            boolean continueReading = super.onStreamPacket(handle, timestamp, len, data);

            // process data only if within the given time limits and that parent function
            // returned an 'ok'/'continue reading' upon analyzing the data
            if ((continueReading == CONTINUE_READING) && afterRequestedTimeIntervalStart()) {
                // todo: if scanning only, packet parsing could be simplified (libPCARD does not support that yet)
                RawDataStreamPacket packet = new RawDataStreamPacket(data, timestamp);
                UnpackedEcgData unpackedEcgData = fileScanResults.dataStream.unpack(packet);

                boolean outputWasFlushed = false;
                if (performScanOnly) {
                    S2PacketForTimeAlign p = new S2PacketForTimeAlign(unpackedEcgData.times[0], unpackedEcgData.rawSampleCounter);
                    outputWasFlushed = timeAlign.registerPacket(p);
                } else {
                    FullS2Packet p = new FullS2Packet(unpackedEcgData.times[0], unpackedEcgData.rawSampleCounter, unpackedEcgData.values);
                    outputWasFlushed = timeAlign.registerPacket(p);
                }

                // return true (keep reading s2) if cache was not flushed yet (not full yet), false (pause reading, to process the output) otherwise
                return outputWasFlushed ? INTERRUPT_READING : CONTINUE_READING;
            } else {
                return INTERRUPT_READING;
            }
        }

        @Override
        public boolean onMetadata(String key, String value) {
            boolean ret = super.onMetadata(key, value);
            if (fileScanResults.metadata == null) {
                fileScanResults.metadata = new HashMap<>(20);
            }
            fileScanResults.metadata.put(key, value);
            return ret;
        }

        @Override
        public boolean onSpecialMessage(char who, char what, String message){
            boolean result = super.onSpecialMessage(who, what, message);

            // do not parse annotations when performing a data load loop
            if (!performScanOnly)
                return result;

            if (what=='a')
                fileScanResults.annotations.add(new AnnotationData(s2.getTimeState(), who, message));
            return result && CONTINUE_READING;
        }

        public void finalizeFileScanResults() {
            fileScanResults.recordingDeviceSoftware = recordingDeviceSoftware;
            fileScanResults.gadgetFirmwareAndHardware = gadgetFirmwareAndHardware;
        }
    }

    /**
     * The only constructor
     * @param inputFname  input .s2 file
     */
    public S2Load(String inputFname) {
        loadFilenameDataStream = inputFname;
    }

    /**
     * Close opened files, and clean objects used
     * Object is no longer usable after calling this function
     */
    public void close(){
        //TODO what more can we discard? can we close opened file?
        s2LoadCallback = null;
        scanResults = null;
    }

    /**
     * If resampling should be preformed, then specify the resampler with this function before calling convertLoop.
     * @param resampler the instance of resampler to use; can be null to disable resampling
     */
    public void setResampler(ResampleInterface resampler) {
        this.resampler = resampler;
    }

    /**
     * Use this function to limit the memory requirements of converter.
     *
     * The default value is 100000, which should result in max of some 10 MB RAM being used.
     *
     * @param targetNumSamplesInChunk the target number (actual number will differ by a few samples)
     */
    public void setTargetNumberOfSamplesPerIteration(int targetNumSamplesInChunk) {
        this.targetNumSamplesInChunk = targetNumSamplesInChunk;
    }

    /**
     * Use this function to limit the interval of s2 file to read from; limit by time [s]
     * @param targetStartTime start time [s] limit
     * @param targetEndTime   end time [s] limit
     */
    public void limitInputTimeInterval(double targetStartTime, double targetEndTime) {
        this.targetStartTime = targetStartTime;
        this.targetEndTime = targetEndTime;
    }

    /**
     * Resample the samples in alignedSamples member variable to target frequency.
     */
    private void resampleInputBuffer() {
        // note: if resampler factor differs from 1 then resampler cannot be null
        if (resamplerFactor != 1)
            resampler.resample(alignedSamples, resampledSamples);
        else
            resampledSamples = alignedSamples;
    }

    /**
     * Convert a list of packets (multiple samples with counter and timestamp) to continuous timeseries
     * @param packets     input list of packets, eac packet must be of type FullS2Packet
     * @param targetIndex expected index of the first packet - actual index being larger indicates missing data
     * @return a timeseries corresponding to input packets - missing data is filled with NaNs
     */
    private float[] packetsToTimeSeries(PcardTimeAlignment.DataCollection<S2PacketForTimeAlign> packets, long targetIndex) {
        if (packets.size() == 0)
            return null;

        try {
            // copy some often used values to variables
            FullS2Packet lastP = (FullS2Packet) packets.get(packets.size() - 1);
            int numOutputSamples = (int)(lastP.getCounter() - targetIndex) + lastP.data.length;

            // prepare output array
            float output[] = new float[numOutputSamples];

            // target index relative to the start of this chunk of packets
            int relativeTargetIndex = 0;

            // copy data from packets to the output array
            for (S2PacketForTimeAlign p : packets) {
                // first sample index of this packet - it ie expected to be equal targetIndex; if not there is missing data
                int firstIndex = (int)(p.getCounter()-targetIndex);

                // Note: firstIndex must not be negative, if it is:
                //  - there is an error in time alignment
                //  - following code will raise an exception (no good catching this, since
                //      something up the hierarchy might be broken - at least time align is)

                // fill in any missing data (between targetIndex and firstIndex)
                if (firstIndex > relativeTargetIndex) {
                    for (int i = relativeTargetIndex; i<firstIndex; ++i) {
                        // TODO: make missing values something else than NAN before resampling
                        output[i] = Float.NaN;
                    }
                }

                // copy the packet data
                FullS2Packet fp = (FullS2Packet) p;
                for (int i = 0; i < fp.data.length; ++i) {
                    output[firstIndex + i] = fp.data[i];
                }

                // modify targetIndex to target the next packet
                relativeTargetIndex = firstIndex + fp.data.length;
            }

            return output;
        } catch (ClassCastException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Helper class that scans over s2 file, in order to calculate the ECG time series length.
     *
     * The code here is otherwise used only in fullConvert function but is in own class since it
     * is best structured into variables and functions
     */
    private class ScanS2ForLength {
        private long firstCounter = -1;
        private long firstTimestamp = 0;
        private long lastCounter = 0;

        /**
         * A function to execute inside the s2 line-reading loop
         *
         * Input to this function is the data in timeAlign buffer, which is filled in the s2 line-reading loop
         * Output are variables firstCounter and lastCounter
         */
        private void scanLoadedDataForPackets() {
            // one buffer full, read its length, clear it, go on
            PcardTimeAlignment.DataCollection<S2PacketForTimeAlign> packets = timeAlign.getOutputData();

            // sometimes the loop will be interrupted with empty output, make sure to detect it to just continue the loop
            if (packets == null || packets.size() == 0)
                return;

            long f = packets.get(0).counter;
            // if measurement is not valid yet (no data processed so far) then init firstCounter
            if (!measurementIsValid()) {
                firstCounter = f;
                firstTimestamp = packets.get(0).timestamp;
            }
            lastCounter = packets.get(packets.size() - 1).counter;
            logger.info("reading lines, currently at interval "+firstCounter+" - "+(lastCounter+s2StreamType.getNumSamplesPerPacket()-1));
            packets.clear();
        }

        /**
         * Perform a full scan of s2 file, using the provided callback.
         *
         * Prerequisites:
         *  s2LoadStatus is created and is valid
         *      s2 file is opened,
         *  callback is initialized
         *  timeAlign is initialized
         *
         * Outputs (member variables of this class):
         *  firstCounter    the first counter (after time alignment) value
         *  lastCounter     the last counter (after time alignment) value
         *
         * @param callback the callback so use for scanning
         */
        void scan(S2LoadCallback callback) {
            callback.performScanOnly(true);
            while (s2LoadStatus.readLines(callback, false)) {
                scanLoadedDataForPackets();
                if (callback.timeIntervalDone())
                    break;
            }
            timeAlign.stop();
            // call scan for packets again to process the last output buffer of the time align instance
            scanLoadedDataForPackets();
            // finalize file scan results (some metadata has to be copied over)
            callback.finalizeFileScanResults();
        }

        /**
         * Get the first counter of read data;
         * @return the first valid counter or invalid value (-1) if measurement is invalid
         */
        public long getFirstCounter() {
            return firstCounter;
        }

        /**
         * Get the first timestamp (after time alignment) of read data [ns];
         * @return the timestamp of the first sample
         */
        public long getFirstTimestamp() {
            return firstTimestamp;
        }

        /**
         * Get the last counter of read data - not the last packet counter but the last sample counter
         * @return the last valid counter or invlaid value (-1) if measurement is invalid
         */
        public long getLastCounter() {
            return lastCounter + (measurementIsValid() ? s2StreamType.getNumSamplesPerPacket()-1 : -1);
        }

        /**
         * Test if first counter is valid and some measurement was therefore collected
         * @return true if there is some measurement already processed
         */
        public boolean measurementIsValid() {
            return firstCounter >= 0;
        }
    }

    /**
     * Helper class that provides conversion functionality.
     *
     * The code here is otherwise used only in fullConvert function but is in own class since it
     * is best structured into variables and functions
     */
    private class ConvertFile {
        private long firstCounter = -1;
        private long targetIndex = 0;
        private long targetResampledIndex = 0;
        private long numResampledSamples = 0;

        private boolean requestToTerminate = false;

        private IterativeConvertCallback conversionCallback;

        ConvertFile(IterativeConvertCallback conversionCallback) {
            this.conversionCallback = conversionCallback;
        }

        /**
         * Convert time aligned packets, flatten them to time series, resample and store to ishne
         *
         * The algorithm used to calculate sample times uses large numbers (indices easily run
         * larger than 2^24) which causes problems with floating points. Best way of dealing with
         * this problem is encoding very large indices as integer part, encoded as long, and
         * fraction part, encoded as float.
         *
         */
        void convertPackets() {
            // one buffer full, read its length, clear it, go on

            // resample and clear the list of packets so that the s2 reading loop may continue
            PcardTimeAlignment.DataCollection<S2PacketForTimeAlign> timeAlignedData = timeAlign.getOutputData();

            // sometimes the main loop (where this function is called from) will be interrupted with
            // empty timeAlign output, make sure to detect such cases and just return to the loop
            if (timeAlignedData == null || timeAlignedData.size() == 0)
                return;

            // prepare a timestream for resampling
            final long f = timeAlignedData.get(0).getCounter();
            final long l = timeAlignedData.get(timeAlignedData.size() - 1).getCounter();
            if (targetIndex == 0) {
                // first packet detected, this one is special as all the initial missing data is simply skipped
                targetIndex = f;
                // target resampled index must be larger or equal targetIndex -> it is interpolation point of data that starts at target index
                targetResampledIndex = (long) Math.ceil(f / resamplerFactor);
            }
            //alignedSamples = packetsToTimeSeries(timeAlignedData, targetIndex);
            alignedSamples = packetsToTimeSeries(timeAlignedData, targetIndex);

            logger.fine("one buffer of size "+timeAlignedData.size()+" aligned: ["+f+", "+(l+s2StreamType.getNumSamplesPerPacket()-1)+"]");
            // resample and clear the list of packets so that the s2 reading loop may continue

            int numResampledValues = 0;
            if (resamplerFactor != 1) {
                // start offset is the offset of first interpolated sample in the 0-based indexing of current data chunk (must be positive)
                double startOfs = targetResampledIndex * resamplerFactor - targetIndex;

                if (startOfs < 0)
                    throw new RuntimeException("debug termination: negative start offset for resampler");

                // number of resampled values in this iteration, all resampled values must fit within the sampled data
                numResampledValues = (int) ((alignedSamples.length - startOfs) / resamplerFactor) + 1;
                resampledSamples = new float[numResampledValues];
                // fill in time values for resampler to turn into sample values
                for (int i = 0; i < resampledSamples.length; ++i)
                    resampledSamples[i] = (float)(startOfs + resamplerFactor * i);

                logger.info(String.format(Locale.US, "Resampler. input: %d samples, output: %d, indices: %.3f - %.3f",
                        alignedSamples.length, resampledSamples.length, resampledSamples[0], resampledSamples[resampledSamples.length - 1]));

                if (debug_everything && (resampledSamples[0] < 0))
                    throw new RuntimeException("debug termination: negative sample index for res<mpler");

                resampleInputBuffer();
            } else {
                resampledSamples = alignedSamples;
                numResampledValues = alignedSamples.length;
            }
            numResampledSamples += resampledSamples.length;
            timeAlignedData.clear();

            if (conversionCallback != null) {
                requestToTerminate = !conversionCallback.onIteration(resampledSamples);
            } else {
                throw new RuntimeException("Callback not defined when performing a conversion loop");
            }

            // fix target index to target the next chunk of data and all other incremental variables
            targetIndex += alignedSamples.length;
            targetResampledIndex += numResampledValues;
        }

        /**
         * Perform a full data convert procedure
         *
         * Prerequisites:
         *  s2LoadStatus    is created and is valid (can be rewound after a scan)
         *      s2 file     is opened,
         *  callback        is initialized
         *  timeAlign       is initialized
         *  ishneStore      is initialized (that's the incremental ishne file storing object)
         *  resampler       is initialized
         *
         * Outputs (member variables of this class):
         *  firstCounter    the first counter (after time alignment) value
         *  lastCounter     the last counter (after time alignment) value
         *
         * @param callback the s2 callback instanace (this could be a member)
         */
        void convert(S2LoadCallback callback) {
            callback.performScanOnly(false);
            while (s2LoadStatus.readLines(callback, false)) {
                convertPackets();
                if (shouldStop())
                    break;
                if (callback.timeIntervalDone())
                    break;
            }
            timeAlign.stop();
            convertPackets();
        }

        public boolean shouldStop() {
            return requestToTerminate;
        }
    }

    /**
     * Interface to implement processing for function convertLoop
     */
    public interface IterativeConvertCallback {
        /**
         * Select the output frequency, given the frequency of the input s2 file;
         * Resampling will be performed if the output frequency does not match the input. Otherwise
         * the data will be passed through directly.
         *
         * @param inputFrequency frequency of ECG sampling in the s2 file
         * @return the requested frequency of output ECG signal
         */
        float selectFrequency(float inputFrequency);
        /**
         * This callback will be called after the input file is scanned and some parameters known.
         * @param scanResult a collection of variables that were initialized with infro from scan phase
         */
        void onInitialization(FileScanResults scanResult);
        /**
         * This function will be called on every iteration of iterative load (convertLoop)
         * @param samples the samples that were read during the last iteration
         * @return false to signal no more processing will be done and s2 loading function can
         * also terminate,
         */
        boolean onIteration(float samples[]);
        /**
         * This method will be called when conversion is done to complete the conversion.
         */
        void complete();
    }

    /**
     * Perform a file scan - read number of samples, types of streams, sampling frequency etc.
     * @param callback a callback to pass results to (selectFrequency and onInitialization)
     * @return instance of scan results (to be observed, not modified)
     */
    public FileScanResults scan(IterativeConvertCallback callback) {
        scanStartTime = System.currentTimeMillis();

        // s2; always create new instances
        s2LoadFile = new S2();
        s2LoadStatus = s2LoadFile.load(new File(loadFilenameDataStream));
        if (!s2LoadStatus.isOk()) {
            logger.warning("Failed to load S2");
            logger.info(s2LoadFile.getNotes());
            return null;
        }

        // time align (use the same size buffer as for conversion!); assume 14 samples per packet - doesn't matter much if this is wrong
        timeAlign.setBufferSize(targetNumSamplesInChunk / 14);

        logger.info("Starting a scan of '"+loadFilenameDataStream+"'...");
        s2LoadCallback = new S2LoadCallback(s2LoadFile);
        if (targetEndTime <= targetStartTime)
            targetEndTime = Long.MAX_VALUE;
        s2LoadCallback.setTimeIntervalInNanos((long)(targetStartTime*1e9), (long)(targetEndTime*1e9));

        // scan the input file
        ScanS2ForLength scanHelper = new ScanS2ForLength();
        scanHelper.scan(s2LoadCallback);
        scanResults = s2LoadCallback.fileScanResults;

        scanResults.inputFrequency = s2LoadFile.getCachedHandles()['e'].sensorDefinition.samplingFrequency;
        // sampling frequency to resample the input to [Hz]
        float outputSamplingFrequency = callback.selectFrequency(scanResults.inputFrequency);

        // TODO: make proper definitions in libPcard for normal and extended ecg stream and use them here
        if (resampler != null)
            resamplerFactor = scanResults.inputFrequency / outputSamplingFrequency;
        else
            resamplerFactor = 1;

        // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // output/callback
        // initialize with linear transformation to nanoVolts!
        scanResults.linearTransformationFunction = s2LoadCallback.getLinearTransformationForEcgSamples();

        // total number of read samples and calculated number of samples to write
        scanResults.inputSampleCounterStart = scanHelper.getFirstCounter();
        scanResults.inputSampleCounterEnd = scanHelper.getLastCounter();
        if (scanHelper.measurementIsValid()) {
            scanResults.numInputSamples = scanHelper.getLastCounter() - scanHelper.getFirstCounter() + 1;
        } else {
            scanResults.numInputSamples = 0;
        }

        // measurement start/end in naoseconds
        scanResults.startTimeNanos = scanHelper.getFirstTimestamp();
        long totalMeasurementLenght = (long)((scanHelper.getLastCounter()-scanHelper.getFirstCounter()) * (1e9 / scanResults.inputFrequency));
        scanResults.endInputNanos = scanResults.startTimeNanos + totalMeasurementLenght;

        // calculate number of samples after resampling
        if (scanHelper.measurementIsValid()) {
            scanResults.targetNumSamples = (long)(
                    Math.floor(scanResults.inputSampleCounterEnd / resamplerFactor)-
                    Math.ceil(scanResults.inputSampleCounterStart / resamplerFactor)
                     + 1
            );
        } else
            scanResults.targetNumSamples = 0;

        scanResults.recordDateTime = s2LoadCallback.getStartDateTime();

        logger.info(String.format(Locale.US, "Scan complete: counted %d (%d-%d) samples", scanResults.numInputSamples, scanResults.endInputNanos, scanResults.startTimeNanos));
        callback.onInitialization(scanResults);
        scanEndTime = System.currentTimeMillis();

        return scanResults;
    }

    /**
     * Function that performs the conversion in a loop, until fully converted
     *
     * Procedure:
     *  - create and setup S2 instance for loading
     *  - create TimeAlignment instance to align packets as hey are read from s2
     *  - create Resampler to resample packets after they are read
     *  - create IshneFile instance for storing
     *  - setup loop:
     *      - read lines until first data stream line is encountered.
     *  - setup TimeAlignment instance, using sampling frequency
     *  - scanning loop:
     *      - read lines until end of file
     *      - pass timestamps and counters from data stream 0 to TimeAlignment instance
     *  - use TimeAlignment instance to calculate total data length
     *  - setup Resampler using sampling frequency
     *  - setup IshneFile instance using sampling frequency and total data length
     *  - conversion loop:
     *      - rewind file and read until end of file
     *      - pass data to TimeAlign instance,
     *          then its data to Resampler instance
     *          then its data to IshneFile instance
     *
     * @param callback the callback to use while looping
     */
    public void convertLoop(IterativeConvertCallback callback) {
        if (scanResults == null)
            throw new RuntimeException("Error: 'scan' was not either called prior to 'convertLoop' or failed to execute");
        conversionLoopStartTime = System.currentTimeMillis();

        // s2 variable must be full initialized at this point (scan initializes them)
        logger.info("Converting '"+loadFilenameDataStream+"'...");
        s2LoadStatus.rewindFile();
        timeAlign.init();
        s2LoadCallback.reset();
        ConvertFile convertFile = new ConvertFile(callback);
        convertFile.convert(s2LoadCallback);

        // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // output/callback
        callback.complete();

        // check if everything is ok
        boolean everythingOk = s2LoadFile.getNumErrors() == 0;
        if (!everythingOk) {
            logger.info(s2LoadFile.getNotes());
        }

        logger.fine(String.format(Locale.US, "Number of processed lines in file %s = %d", loadFilenameDataStream, s2LoadFile.getNumProcessedLines()));
        if (scanResults.targetNumSamples != convertFile.numResampledSamples)
            logger.warning(String.format(Locale.US, "Scanned size (%d) differs from the actual converted size (%d) by %d.",
                    scanResults.targetNumSamples, convertFile.numResampledSamples, scanResults.targetNumSamples - convertFile.numResampledSamples));

        s2LoadStatus.closeFile();
        conversionLoopEndTime = System.currentTimeMillis();
    }

    /**
     * Get the timing results - conversion time in milliseconds.
     * @return conversion time [ms]
     */
    public long getTotalScanTimeInMillis() {
        return (scanEndTime - scanStartTime);
    }

    /**
     * Get the timing results - conversion time in milliseconds.
     * @return conversion time [ms]
     */
    public long getTotalConversionTimeInMillis() {
        return (conversionLoopEndTime - conversionLoopStartTime);
    }
}
