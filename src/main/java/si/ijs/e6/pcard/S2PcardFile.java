package si.ijs.e6.pcard;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;


import si.ijs.e6.S2;


public class S2PcardFile {
    /**
     * PCARD data streams
     * note that some constants are copied from Pcard namespace, since they are defined more globally.
     */
    public static final int     ECG_SAMPLE_HANDLE = (int)'e';
    public static final String  ECG_SAMPLE_HANDLE_NAME = Pcard.ECG_SAMPLE_NAME;
    public static final String  ECG_SAMPLE_HANDLE_UNIT = Pcard.ECG_SAMPLE_UNIT;

    public static final int     ECG_SAMPLE_COUNTER_HANDLE = (int)'c';
    public static final String  ECG_SAMPLE_COUNTER_HANDLE_NAME = Pcard.ECG_SAMPLE_COUNTER_NAME;
    public static final String  ECG_SAMPLE_COUNTER_HANDLE_UNIT = Pcard.ECG_SAMPLE_COUNTER_UNIT;
    public static final String  ECG_STREAM_NAME = Pcard.ECG_STREAM_NAME;

    public static final String  NORMAL_DATA_STREAM_STRUCTURE = "eeeeeeeeeeeeeec";
    public static final String  EXT_DATA_STREAM_STRUCTURE = "eeeeeeeeeeeec";
    public static final int     NORMAL_DATA_STREAM_HANDLE = 0;
    // ***************************************************************************************************

    /**
     * Load/store disconnect events
     */
    public static class DisConnectEvents {
        /**
         * (Dis)Connect message strings will start with the following character sequences.
         */
        public static final String  CONNECTED = "recording hardware connected",
                                    DISCONNECTED = "recording hardware disconnected";
        /**
         * (Dis)connecd is messaged through specific device and message types
         */
        public static S2.DeviceType deviceType = S2.DeviceType.dt_sensorDevice;
        public static S2.MessageType messageType = S2.MessageType.mt_note;


        enum MessageIs {
            Other,
            Connect,
            Disconnect,
        }

        public static MessageIs isMessageDisOrConnect(final char who, final char what, final String message) {
            if ((deviceType.byteId == who) && (messageType.byteId == what)) {
                if (message.startsWith(CONNECTED))
                    return MessageIs.Connect;
                if (message.startsWith(DISCONNECTED))
                    return MessageIs.Disconnect;
            }
            return MessageIs.Other;
        }
    }

    /**
     * S2 file version related constants and checks
     */
    public static class Version {
        /**
         * This is the extended version string that all files should be stored under.
         * Note: if this is to change in the future (not planned), then loading old files should
         * remain possible, since there is a list of validExtendedVersions, but storing to old
         * format through the use of this class will not be supported.
         */
        public static final String targetExtendedVersion = "PCARD";
        /**
         * This is the base version integer that all files should be stored under.
         * Note: if this is to change in the future (which is likely), then loading old files should
         * remain possible, since there is a list  validS2Versions, but storing to old format
         * through the use of this class will not be supported.
         */
        public static int targetBaseVersion = 1;
        /**
         * Valid versions of S2 file
         *
         * Note: must be sorted in ascending order.
         */
        public static int validS2Versions[] = {1};
        /**
         * Valid extended s2 file versions
         */
        public static final HashSet<String> validExtendedVersions = new HashSet<>(Arrays.asList("PCARD"));

        /**
         * Verify that the provided set of int, string (as found in s2 file) is a valid PCARD file
         * @param versionInt      the integer base version
         * @param extendedVersion the string extended version
         * @return true if the specified version defines a valid PCARD s2 file
         */
        public static boolean isValidPcard(final int versionInt, final String extendedVersion) {
            return ((Arrays.binarySearch(validS2Versions, versionInt) >= 0) &&
                    (validExtendedVersions.contains(extendedVersion)));
        }
    }

    /**
     * All metadata entries in valid s2 PCARD file
     *
     * The strings below were converted to enum using the following gawk command:
     * {@literal '/public/{out=""; for(i=7;i<=NF;i++){out=out" "$i}; out1=substr(out, 1, length(out)-1); print "        "$5"(" out1 "),";} /\/\//'}
     * then the case was also changed...
     */
    @Deprecated
    public static class MetadataStrings {
        //
        //region basic set
        public static final String RECORDING_DEVICE_SOFTWARE =      "recording software";
        public static final String SENSING_GADGET_HARDWARE =        "recording hardware";
        public static final String MAC_ADDRESS =                    "MAC";
        public static final String RECORDING_DATE =                 "date";
        public static final String RECORDING_TIME =                 "time";
        public static final String RECORDING_TIMEZONE =             "timezone";
        //endregion

        //region extended set (incrementally added prior to Savvy1.3)
        public static final String SELECTED_ADC_CHANNEL =           "ADC channel";
        public static final String DEBUG_SELECTED_ANALOG_AMPL =     "DEBUG Analog amplifier select";
        public static final String DEBUG_SELECTED_ECG_RANGE =       "DEBUG ECG range";
        public static final String DEBUG_CALIBRATION_COEFFS =       "DEBUG calibration (ofs, 2mV amp)";
        public static final String FACTORY_CALIBRATION_VALUES =     "factory calibration values";
        public static final String SELECTED_TRANSFER_PROTOCOL =     "Transfer protocol";
        // recording device = the device that records measurement to storage (in contrast to sensing device that contains the sensors)
        public static final String RECORDING_DEVICE_MODEL =         "Recorder device model";
        public static final String RECORDING_DEVICE_OS =            "Recorder device OS";
        public static final String RECORDING_DEVICE_SERIAL_NUMBER = "Recorder device SN";
        //endregion

        //region optional (development from non-develop/master/Savvy branch)
        public static final String THINK_EHR_ID =                   "ehrid";
        public static final String SENSING_GADGET_LOCATION =        "gadget location";
        //endregion
    }

    /**
     *
     * To get a complete list of enums, run one of the following gawk scripts on the below text:
     * '/"/{print substr($1,1,length($1)-1)}'
     * '/"/{print "case " substr($1,1,length($1)-1) ": \nbreak;"} '
     */
    public enum Metadata {
        //legacy (no longer used)
        Gadget_firmware_revision("Gadget firmware revision"),   // 1.0.1e 279d848
        //endregion

        //region basic set
        Recording_device_software( "recording software"),   // MobECG 1.8.1-69-g616eabfa (master)
        Sensing_gadget_hardware( "recording hardware"),     // "Savvy fw=1.0.5, hw='Savvy 1.3', git hash=c83c491"  or  
                                                            // "Savvy 1.0.2e 1b34dca"
        Mac_address( "MAC"),                                // C9:25:32:C4:B1:F7
        Recording_date( "date"),                            // 2019-05-08
        Recording_time( "time"),                            // 17:03:48.315
        Recording_timezone( "timezone"),                    // +0200
        //endregion

        //region extended set (incrementally added prior to savvy1.3)
        Selected_adc_channel( "adc channel"),               // 0
        Debug_selected_analog_ampl( "debug analog amplifier select"),   // 0
        Debug_selected_ecg_range( "debug ecg range"),       // 6.0
        Debug_calibration_coeffs( "debug calibration (ofs, 2mv amp)"),  // 513.94, 323.00
        Factory_calibration_values( "factory calibration values"),
        Selected_transfer_protocol( "transfer protocol"),   // 0
        // recording device = the device that records measurement to storage (in contrast to sensing device that contains the sensors)
        Recording_device_model( "recorder device model"),   // Asus Nexus 7
        Recording_device_os( "recorder device os"),         // Android 6.0.1
        Recording_device_serial_number( "recorder device sn"),  // 07260874
        //endregion

        //region optional (development from non-develop/master/savvy branch)
        Think_ehr_id( "ehrid"),
        Sensing_gadget_location( "gadget location"),
        //endregion
        Invalid("\0\0");


        String keyInS2;
        static Map<String, Metadata> mapOfPossibleValues = new HashMap<>();

        static {
            for(Metadata md : Metadata.values())
                mapOfPossibleValues.put(md.keyInS2, md);
        }

        Metadata(String key) {
            keyInS2 = key;
        }

        /**
         * Maps the string key to Metadata instance, returns Metadata.Invalid if the key cannot be
         * mapped to any enum instance.
         *
         * @param key string that represents the key
         * @return Metadata instance that corresponds to the key or Invalid
         */
        public static Metadata fromString(String key) {
            return mapOfPossibleValues.containsKey(key) ? mapOfPossibleValues.get(key) : Invalid;
        }

        /**
         * Return the string representation of this enum
         * @return the string representation (human readable)
         */
        public final String getString() {
            return keyInS2;
        }
    }

    /**
     * When s2 reads metadata of type 'Sensing_gadget_hardware', the value can be passed here
     * to translate to Hardware enum instance.
     *
     * @param metadataValue the value to parse and split to gadget name and firmware instance
     * @return instance of {@link Pcard.Hardware} enum that corresponds to input string or Undefined
     */
    public static FirmwareRevision parseMetadataString(String metadataValue) {
        // split the input string by spaces and decide if known hardware, based on the first entry
        String[] firstValueSplit = metadataValue.split("\\s", 2);
        {
            if (firstValueSplit[0].equalsIgnoreCase("Savvy") || firstValueSplit[0].equalsIgnoreCase("Mobecg"))
                return FirmwareRevision.parseHumanReadableString(firstValueSplit[1]);
            else
                return FirmwareRevision.parseHumanReadableString(metadataValue);
        }
        //return new FirmwareRevision("");
    }

    /**
     * Get the best linear function, given the existing function (or null), and firmware instance.
     * S2 files, where calibration constants are provided will give good linear transformation
     * functions, others may not. This function is used to decide when to use what.
     *
     *
     * @param input    existing linear transformation function - read from s2 definition - or null
     * @param firmware the firmware as read from s2 file
     * @return a best guess at linear transformation function
     */
    public static LinearTransformationFunction getBetterLinearTransformationFunction(LinearTransformationFunction input, FirmwareRevision firmware) {
        // check if inputs are null, and give up if both are null
        if (firmware == null) {
            if (input == null)
                throw new RuntimeException("Cannot deduce linear transformation function from null");
            else
                return input;
        }
        // at this point, firmware must be defined

        // if only input is undefined, use function defined by the hardware (even if hardware is undefined)
        if (input == null) {
            // firmware has hardware variable always defined, no need to check for null
            return firmware.getHardware().linearTransformationFunction;
        }
        // at this point both variables - firmware and input - are defined

        // if input only has k defined, recalculate n from it and from hardware defined function
        if (input.getN() == 0) {
            LinearTransformationFunction f = firmware.getHardware().linearTransformationFunction;
            input.setN(f.getN() / f.getK() * input.getK());
        }

        // most generic case, nothing is wrong with how input is given, therefore just return it:
        return input;
    }
}
