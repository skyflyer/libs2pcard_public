package si.ijs.e6.pcard;

import java.util.ArrayList;

import si.ijs.e6.S2;
import si.ijs.e6.S2.DataEntityCache;
import si.ijs.e6.S2.DeviceType;
import si.ijs.e6.S2.MessageType;
import si.ijs.e6.S2.Nanoseconds;
import si.ijs.e6.S2.SensorDefinition;
import si.ijs.e6.S2.StoreStatus;
import si.ijs.e6.S2.StructDefinition;
import si.ijs.e6.S2.TimestampDefinition;

import static si.ijs.e6.pcard.S2PcardFile.ECG_SAMPLE_HANDLE;


/**
 * This is an implementation of callbacks for S2.LoadStatus that performs PCARD specific parsing.
 *
 * All lines will be parsed and processed by this callback. A list of entries is managed that
 * contains parsed/processed lines for faster consumption.
 *
 * TODO: unit test this callback
 *
 * The following parsing will be performed:
 * <ul>
 *     <li>Everything that {@link PcardBasicCallback} does</li>
 *     <li>Data processing(TODO: currently it lacks proper algorithm)</li>
 *     <li>Data mapping (raw to the form prescribed in file description)</li>
 *     <li>Processing of counters (very basic - does not fully support the normal transformedData stream)</li>
 *     <li>Conversion of transformedData to {@link PcardStreamPacket}</li>
 *     <li>Stored entries can be used to write a new s2 file</li>
 * </ul>
 *
 * @author Matjaž
 */
public class PcardStoreAllCallback extends PcardBasicCallback {
    private static final int UNKNOWN_LINE = -1;
    // enable transformedData mapping as specified in the s2 sensor description; if false, transformedData will be left in raw form
    private boolean dataMapping = false;
    // process transformedData (parse it and store to internal cache) or not
    private boolean dataProcessing = true;
    // process and store counters (separate from transformedData) of each incoming packet
    private boolean counterProcessing = true;

    public PcardStreamPacket lastParsedStreamPacket = null;


    /**
     * Interface for storing the s2 lines; All entries support this interface.
     */
    public interface Entry {
        void writeLine(StoreStatus s);
    }

    /**
     * Virtual class that adds timestamp to the Entry interface
     */
    abstract static class TimestampedEntry implements Entry {
        long timestamp;

        /**
         * Basic ctor takes timestamp
         * @param nanosTimestamp    timestamp in nanoseconds
         */
        public TimestampedEntry(long nanosTimestamp) {
            this.timestamp = nanosTimestamp;
        }

        @Override
        public abstract void writeLine(StoreStatus s);

        /**
         * This returns the timestamp of comment in nanoseconds
         * @return this entry's timestamp
         */
        public long getNanos() {
            return this.timestamp;
        }
    }

    /**
     * This class implements a simple comment
     */
    public static class Comment extends TimestampedEntry {
        private String comment;

        /**
         * This constructs a comment with a specified timestamp and comment
         * @param timestamp the timestamp of the comment
         * @param comment the content of the comment
         */
        public Comment(long timestamp, String comment) {
            super(timestamp);
            this.comment = comment;
        }

        /**
         * This returns comment's string value
         * @return this comment's value
         */
        public String getComment() {
            return this.comment;
        }

        /**
         * writing comment to s2 will not append a timestamp (comments are not managed by PCARD standard)
         * @param s the associated {@link S2.StoreStatus} instance to be used for storing
         */
        public void writeLine(StoreStatus s) {
            s.addTextMessage(comment);
        }
    }

    /**
     * This class implements a special message
     */
    public static class SpecialMessage extends TimestampedEntry {
        private String message;
        private DeviceType dt;
        private MessageType mt;

        /**
         * This constructs a special message with specified timestamp, who, what and message
         * @param timestamp the timestamp of the special message of ns
         * @param who the special message sender
         * @param what the type of message
         * @param message the content of message
         */
        public SpecialMessage(long timestamp, char who, char what, String message) {
            super(timestamp);
            this.message = message;
            this.dt = DeviceType.convert((byte)who);
            this.mt = MessageType.convert((byte)what);
        }

        /**
         * This returns the content of special message
         * @return this special message's content
         */
        public String getMessage() {
            return this.message;
        }

        /**
         * This returns human readable sender value
         * @return this special message's human readable sender
         */
        public DeviceType getDeviceType() {
            return this.dt;
        }

        /**
         * This returns human readable type value
         * @return this special message's human readable message type
         */
        public MessageType getMessageType() {
            return this.mt;
        }

        public void writeLine(StoreStatus s) {
            s.addSpecialTextMessage(dt, mt, message);
        }
    }

    /**
     * This class implements the error
     */
    public static class Error extends TimestampedEntry {
		private int lineNum;
		private String error;

        /**
         * Basic constructor
         * @param lineNum   s2 lineNumber, where the error appears
         * @param error     human readable error string
         * @param timestamp s2 timestamp, hen the error appears
         */
        public Error(int lineNum, String error, long timestamp) {
            super(timestamp);
            this.lineNum = lineNum;
            this.error = error;
        }

        /**
         * Get the actual string describing the error.
         * @return the human readable error string
         */
        public String getErrorString() {
            return error;
        }

        /**
         * Storing a loading error to file? Make it a comment.
         * @param s    StoreStatus instance to store the line to.
         */
        public void writeLine(StoreStatus s) {
            if (lineNum > 0)
                s.addTextMessage("error on original line "+lineNum+": "+error);
            else
                s.addTextMessage("error in original file: "+error);
        }
    }

    /**
     * This class implements metadata
     */
    public static class Metadata implements Entry {
        private String key;
        private String value;

        /**
         * This constructs a metadata with a specified key and value
         * @param key the key of metadata
         * @param value the value of metadata
         */
        public Metadata(String key, String value) {
            this.key = key;
            this.value = value;
        }

        /**
         * This returns key of metadata
         * @return this metadata's key
         */
        public String getKey() {
            return this.key;
        }

        /**
         * This returns value of metadata
         * @return this metadata's value
         */
        public String getValue() {
            return this.value;
        }

        public void writeLine(StoreStatus s) {
            s.addMetadata(key, value);
        }
    }

    /**
     * This class implements metadata
     */
    public static class DefinitionEntry implements Entry {
        StructDefinition structDefinition;
        SensorDefinition sensorDefinition;
        TimestampDefinition timestampDefinition;
        byte handle;

        /**
         * Constructor of struct definition
         * @param handle        target entry index
         * @param definition    definition of structure
         */
        public DefinitionEntry(byte handle, StructDefinition definition) {
            this.handle = handle;
            structDefinition = definition;
        }

        /**
         * Constructor of sensor definition
         * @param handle        target entry index
         * @param definition    definition of sensor
         */
        public DefinitionEntry(byte handle, SensorDefinition definition) {
            this.handle = handle;
            sensorDefinition = definition;
        }

        /**
         * Constructor for timestamp definition
         * @param handle        target entry index
         * @param definition    definition of timestamp (for the structure)
         */
        public DefinitionEntry(byte handle, TimestampDefinition definition) {
            this.handle = handle;
            timestampDefinition = definition;
        }

        public void writeLine(StoreStatus s) {
            if (sensorDefinition != null)
                s.addDefinition(handle, sensorDefinition);
            if (structDefinition != null)
                s.addDefinition(handle, structDefinition);
            if (timestampDefinition != null)
                s.addDefinition(handle, timestampDefinition);
        }
    }

    /**
     * Implementation of timestamp entry
     */
    public static class Timestamp extends TimestampedEntry {
        Timestamp(long timestamp) {
            super(timestamp);
        }

        public void writeLine(StoreStatus s) {
            s.addTimestamp(new Nanoseconds(timestamp));
        }
    }

    /**
     * This class implements PCARD stream packet
     */
    public static class PcardStreamPacket implements Entry {
        // TODO 2019
        RawDataStreamPacket dataStreamPacket;

        private S2StreamType type;
        private byte handle;
        private int counter;
        private float[] transformedData;

        /**
         * Construct a stream packet with a specified ecgStreamHandle, raw timestamp, counter and transformedData.
         * This constructor is used in the s2 callback.
         *
         * @param type            the type (enum) of the stream
         * @param handle          the ecgStreamHandle of the stream packet
         * @param rawTimestamp    the raw timestamp of the stream packet
         * @param counter         the counter of the stream packet
         * @param data            the raw data array of the stream packet
         * @param transformedData the transformed (linear transform) array of the stream samples
         */
        public PcardStreamPacket(S2StreamType type, byte handle, long rawTimestamp, int counter, byte[] data, float[] transformedData) {
            this.type = type;
            this.handle = handle;
            this.counter = counter;
            this.transformedData = transformedData;

            dataStreamPacket = new RawDataStreamPacket(data, rawTimestamp);
        }

        /**
         * This returns a ecgStreamHandle of the stream packet
         * @return this stream packet's ecgStreamHandle
         */
        public byte getHandle() {
            return this.handle;
        }

        /**
         * This returns a raw timestamp of the stream packet
         * @return this stream packet's raw timestamp
         */
        public long getRawTimestamp() {
            return dataStreamPacket.receivedNanoTime;
        }

        /**
         * This returns a counter of the stream packet
         * @return this stream packet's counter
         */
        public int getCounter() {
            return this.counter;
        }

        /**
         * Set value for timestamp in nanoseconds (this is not a raw value!)
         * @param rawTimestamp    timestamp in nanoseconds
         */
        public void setTimestampNanos(long rawTimestamp) {
            dataStreamPacket.receivedNanoTime = rawTimestamp;
        }

        /**
         * This returns array transformedData of the stream packet
         * @return this stream packet's array transformedData
         */
        public float[] getTransformedData() {
            return this.transformedData;
        }

        /**
         * Currently implemented only 'normal' transformedData packets (14 samples + counter) that are not mapped into mV.
         *
         * To store mapped (to mV) packets, unmap them first : d_unmapped = (d_mapped-n)/k
         * @param s    target StoreStatus instance
         */
        public void writeLine(StoreStatus s) {
            if (transformedData != null) {
                switch (type) {
                    case NormalStream:
                        // DEBUG output
                        s.addStreamPacketRelative(handle, dataStreamPacket.receivedNanoTime, dataStreamPacket.rawData);
                        break;
                }
            }
        }
    }

    public ArrayList<Entry> entries = new ArrayList<>();
    private byte definedHandles[] = new byte[255];
    // file type is determined when definitions are read and then used when transformedData streams are parsed
    S2StreamType filePacketType = S2StreamType.Undefined;

    /**
     * This constructs a measurementData class with S2 file parameter which specifies transformedData file.
     * @param file Raw transformedData input
     */
    public PcardStoreAllCallback(S2 file) {
        super(file);
        s2 = file;
    }

    /**
     * Enable or disable dataParsing and storing of counters and transformedData, and enable or disable mapping of transformedData.
     * Data mapping value is only meaningful if transformedData dataParsing is enabled.
     * Disabling counters and transformedData is useful for example, if one might only try to determine the coarse length of
     * measurement and would like to avoid all unnecessary processing.
     *
     * @param counterParsing    enable dataParsing and storing of counters
     * @param dataParsing       enable dataParsing and storing of transformedData
     * @param dataMapping       enable mapping of transformedData (according to the rules specified in s2)
     */
    public void setDataParsingAndMapping(boolean counterParsing, boolean dataParsing, boolean dataMapping) {
        counterProcessing = counterParsing;
        dataProcessing = dataParsing;
        this.dataMapping = dataMapping;
    }

    /**
     * Get an array an int array of all the available (declared) stream handles
     * @return the int array of available (declared and read) stream handles
     */
    public int[] getAvailableStreams() {
        if (definedHandles != null) {
            int count = 0;
            for (DataEntityCache handle : s2.getCachedHandles())
                if (handle != null)
                    ++count;

            int handles[] = new int[count];
            count = 0;
            for (int i = 0; i < 255; ++i) {
                if (s2.getCachedHandles()[i] != null) {
                    handles[count] = i;
                    ++count;
                }
            }
            return handles;
        } else {
            return new int[0];
        }
    }

    /**
     * Forward the query to s2: return the time state (variable that changes while lines are being read)
     * @return the value of time state
     */
    public long getTimeState() {
        return s2.getTimeState();
    }

    //region ReadLineCallbackInterface implementation
    @Override
    public boolean onComment(String comment) {
        if (s2.getTimeState() >= super.loadStartNanos)
            entries.add(new Comment(s2.getTimeState(), comment));

        return true;
    }

    @Override
    public boolean onVersion(int versionInt, String extendedVersion) {
        super.onVersion(versionInt, extendedVersion);
        return true;
    }

    @Override
    public boolean onSpecialMessage(char who, char what, String message) {
        //if (lastTimestamp >= loadStartNanos)
        entries.add(new SpecialMessage(s2.getTimeState(), who, what, message));

        return true;
    }

    @Override
    public boolean onMetadata(String key, String value) {
        //commented out for easier tracking of changes during measurement
        //if (lastTimestamp >= loadStartNanos)
        entries.add(new Metadata(key, value));
        return true;
    }

    @Override
    public boolean onEndOfFile() {
        return INTERRUPT_READING;
    }

    @Override
    public boolean onUnmarkedEndOfFile() {
        return INTERRUPT_READING;
    }

    @Override
    public boolean onDefinition(byte handle, SensorDefinition definition) {
        return super.onDefinition(handle, definition);
    }

    @Override
    public boolean onDefinition(byte handle, StructDefinition definition) {
        return super.onDefinition(handle, definition);
    }

    @Override
    public boolean onDefinition(byte handle, TimestampDefinition definition) {
        return super.onDefinition(handle, definition);
    }

    @Override
    public boolean onTimestamp(long nanoSecondTimestamp) {
        if (s2.getTimeState() >= loadStartNanos) {
            entries.add(new Timestamp(nanoSecondTimestamp));
        }
        return (s2.getTimeState() < loadEndNanos);
    }

    @Override
    public boolean onStreamPacket(byte handle, long timestamp, int len, byte data[]) {
        // if this packet is within the selected range of measurement to load, process it

        if (super.onStreamPacket(handle, timestamp, len, data)) {

            // TODO; do some smarter processing (use TimeAlign? use LinearTransformation here or not?)

            if (filePacketType == S2StreamType.NormalStream && (handle == 0)) {
                // if normal packet type is set then handles 0, and 'c', and 'e' are definitely valid, and
                // ecgStreamHandle 0 is a struct of eeeeeeeeeeeeeec, therefore everything below cans be hardcoded

                si.ijs.e6.MultiBitBuffer mbb = new si.ijs.e6.MultiBitBuffer(data);
                float[] sData = null;

                // if transformedData should be parsed and processed, then process the transformedData in the block below
                if (dataProcessing) {
                    int mbbOffset = 0;
                    sData = new float[14];
                    int eResolution = s2.getEntityHandles((byte) ECG_SAMPLE_HANDLE).sensorDefinition.resolution;

                    // process individual samples, mapping them using linear transformation if necessary
                    for (int i = 0; i < 14; ++i) {
                        if (dataMapping)
                            sData[i] = (mbb.getInt(mbbOffset, eResolution) * getLinearTransformation(ECG_SAMPLE_HANDLE).getK() +
                                    getLinearTransformation(ECG_SAMPLE_HANDLE).getN());
                        else
                            sData[i] = mbb.getInt(mbbOffset, eResolution);
                        mbbOffset += eResolution;
                    }
                }
                int sampleCounter = 0;
                if (counterProcessing) {
                    byte cb = (byte) 'c';
                    sampleCounter = (int) (mbb.getInt(140, s2.getEntityHandles(cb).sensorDefinition.resolution) * s2.getEntityHandles(cb).sensorDefinition.k + s2.getEntityHandles(cb).sensorDefinition.n);
                }

                if (dataProcessing || counterProcessing) {
                    lastParsedStreamPacket = new PcardStreamPacket(filePacketType, handle, timestamp, sampleCounter, data, sData);
                    entries.add(lastParsedStreamPacket);
                }
            } else {
                // error: unknown packet type was defined, cannot parse it
                return INTERRUPT_READING;
            }
            return CONTINUE_READING;
        }
        return INTERRUPT_READING;
    }

    @Override
    public boolean onUnknownLineType(byte type, int len, byte data[]) {
        if (s2.getTimeState() >= loadStartNanos) {
            onError(UNKNOWN_LINE, "unknown line type "+(int)(type)+", length="+len);
        }
        return true;
    }

    @Override
    public boolean onError(int lineNum,  String error) {
        if (s2.getTimeState() >= loadStartNanos)
            entries.add(new Error(lineNum, error, s2.getTimeState()));
        return true;
    }
    //endregion

    /**
     * Stores the version (the only non-entry line of the read s2 file)
     * @param the associated {@link S2.StoreStatus} instance to use for storing
     */
    public void writeVersionLine(StoreStatus s) {
        // version is not stored like the other lines, but rather through storeStatus.setVersion(...)
        s.setVersion(s2FileVersion.getIncrementalVersionNumber(), s2FileVersion.getExtendedVersion());
    }
}
