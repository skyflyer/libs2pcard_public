package si.ijs.e6.pcard;


/**
 * "Resamplers" built on this interface should provide methods for resampling regularly sampled data.
 */
public interface ResampleInterface {
    /**
     * Resample input signal according to sample times specified in output and store it in output.
     *
     *
     * @param inputSamples  input samples at virtual sample times 0...inputSamples.length
     * @param outputSamples this is both input and output; at first it should contain virtual sample
     *                      times for the output; the function should then replace these with
     *                      corresponding samples (values of the signal at the given times)
     */
    void resample(float[] inputSamples, float[] outputSamples);
}
