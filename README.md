# Library that combines s2 and PCARD

## Dependencies

User needs to make sure that libpcard is accessible in the directory, or via settings.gradle file point it to correct location

[libPcard](https://gitlab.com/pcard_group/mobecg/libpcard) [imported as submodule]
[libS2](https://gitlab.com/pcard_group/mobecg/libs2) [imported as submodule]

## Provides

### An example of loading raw measurement files (PCARD version of s2)

S2PcardFileRawTest.java provides the example while also serving as a unit test

### class PcardBasicCallback

An implementation of S2.ReadLineCallbackInterface  which provides the s2 reader with basic code for interpreting PCARD files (that is, s2 files of version "x.y.z PCARD")
